package by.yandex.storcheoj;

import by.yandex.storcheoj.detail.Detail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Тест 1");
        Detail q1 = new Detail(1,5);
        Detail q2 = new Detail(6,4);
        Detail q3 = new Detail(2,3);
        Detail q4 = new Detail(6,3);
        Detail q5 = new Detail(9,1);
        Detail q6 = new Detail(4, 5);
        Detail q7 = new Detail(2, 2);
        Detail q8 = new Detail(7, 7);

        List<Detail> listDetail=new ArrayList<>();
        listDetail.add(q1);
        listDetail.add(q2);
        listDetail.add(q3);
        listDetail.add(q4);
        listDetail.add(q5);
        listDetail.add(q6);
        listDetail.add(q7);
        listDetail.add(q8);

        System.out.println("Изначальный список: ");
        for (Detail detail : listDetail) {
            System.out.print(detail+" ");
        }

        System.out.println("Отсортированые детали: ");
        Collections.sort(listDetail);
        for (Detail detail : listDetail) {
            System.out.print(detail+" ");
        }
        System.out.println();
        System.out.println("Время обработки: "+ Detail.productionСycleTime((ArrayList<Detail>) listDetail));
        System.out.println();
        System.out.println("Тест 2");
         q1 = new Detail(12,2);
         q2 = new Detail(7,9);
         q3 = new Detail(3,4);
         q4 = new Detail(5,5);
         q5 = new Detail(1,3);
         q6 = new Detail(7, 5);
         q7 = new Detail(2, 8);
        List<Detail> listDetail2=new ArrayList<>();
        listDetail2.add(q1);
        listDetail2.add(q2);
        listDetail2.add(q3);
        listDetail2.add(q4);
        listDetail2.add(q5);
        listDetail2.add(q6);
        listDetail2.add(q7);


        System.out.println("Изначальный список: ");
        for (Detail detail : listDetail2) {
            System.out.print(detail+" ");
        }

        System.out.println("Отсортированые детали: ");
        Collections.sort(listDetail2);
        for (Detail detail : listDetail2) {
            System.out.print(detail+" ");
        }
        System.out.println();
        System.out.println("Время обработки: "+ Detail.productionСycleTime((ArrayList<Detail>) listDetail2));
        System.out.println();
        System.out.println("Тест 3");
        q1 = new Detail(6,2);
        q2 = new Detail(3,4);
        q3 = new Detail(2,2);
        q4 = new Detail(3,2);

        List<Detail> listDetail3=new ArrayList<>();
        listDetail3.add(q1);
        listDetail3.add(q2);
        listDetail3.add(q3);
        listDetail3.add(q4);



        System.out.println("Изначальный список: ");
        for (Detail detail : listDetail3) {
            System.out.print(detail+" ");
        }

        System.out.println("Отсортированые детали: ");
        Collections.sort(listDetail3);
        for (Detail detail : listDetail3) {
            System.out.print(detail+" ");
        }
        System.out.println();
        System.out.println("Время обработки: "+ Detail.productionСycleTime((ArrayList<Detail>) listDetail3));
        System.out.println();
    }
}
