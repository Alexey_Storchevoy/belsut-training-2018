package by.yandex.storcheoj.detail;

import java.util.ArrayList;

public class Detail implements Comparable <Detail>{
    private int first;
    private int second;
    public Detail(int first1,int second1){
        this.first=first1;
        this.second=second1;
    }

    public int getFirst(){
        return first;
    }
    public int getSecond(){
        return second;
    }
    @Override
    public String toString(){
        return "{" + first + "," + second + "}";
    }
    @Override
    public int compareTo(Detail detail){
        if (this.getFirst() <= this.getSecond() & detail.getFirst() > detail.getSecond()) {
            return -1;
        }
        if (detail.getFirst() <= detail.getSecond() & this.getFirst() > this.getSecond()) {
            return 1;
        }
        if (this.getFirst() <= this.getSecond() & detail.getFirst() <= detail.getSecond()) {
            if (this.getFirst() <= detail.getFirst()) {
                return -1;
            }
            else {
                return 1;
            }
        }
        if (this.getFirst() > this.getSecond() & detail.getFirst() > detail.getSecond()) {
            if (this.getSecond() >= detail.getSecond()) {
                return -1;
            }
            else {
                return 1;
            }
        }
        else {
            return 0;
        }
    }
    public static int productionСycleTime(ArrayList<Detail> listDetail) {
        int process1 = 0;
        int process2 = 0;
        for (Detail detail : listDetail)
        {
            process1 += detail.getFirst();
            if (process1 > process2)
            {
                process2 = process1 + detail.getSecond();
            }
            else
            {
                process2 += detail.getSecond();
            }
        }
        return process1 <= process2 ? process2 : process1;
    }
}
