package by.yandex.storchevoj;

import by.yandex.storchevoj.box.Box;
import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.specificfigure.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Box box=new Box();

        //Создаем фигуры
        PaperTriangle paperTriangle = new PaperTriangle(4);
        PaperCircle paperCircle=new PaperCircle(5);
        PaperRectangle paperRectangle=new PaperRectangle(4.2,3);
        MembraneTriangle membraneTriangle=new MembraneTriangle(5);
        MembraneCircle membraneCircle=new MembraneCircle(3);
        MembraneRectangle membraneRectangle=new MembraneRectangle(2.5,3.8);
        PaperCircle paperRedCircle=new PaperCircle(10);
        //Закрасим бумажные фигуры
        paperTriangle.setColor(Color.BLACK);
        paperCircle.setColor(Color.BLUE);
        paperRectangle.setColor(Color.YELLOW);
        paperRedCircle.setColor(Color.RED);
        //Вырезаем фигуры из фигур
        PaperCircle paperCircleFromTriangle=new PaperCircle(paperTriangle);
        PaperTriangle paperTriangleFromRectangle=new PaperTriangle(paperRectangle);

        //Заполняем коробку
        box.addBox(paperCircle);
        box.addBox(paperRedCircle);
        box.addBox(paperTriangleFromRectangle);
        box.addBox(paperCircleFromTriangle);
        box.addBox(membraneCircle);
        box.addBox(membraneRectangle);
        box.addBox(membraneTriangle);

        //Выведем содежимое коробки
        System.out.println(box.toString());

        //Отсортируем по фигурам
        box.sortByFigure(box.getBox());
        System.out.println(box.toString());


        //Отсортируем по площади
        box.sortByArea(box.getBox());
        System.out.println(box.toString());

        //Отсортируем по цвету
        box.sortByColor(box.getBox());
        System.out.println(box.toString());



        //Добавим список фигур
        List<Figure> listForadd =new ArrayList<>();
        PaperTriangle pt = new PaperTriangle(7);
        PaperCircle pc=new PaperCircle(10);
        PaperRectangle pr=new PaperRectangle(4,6);
        MembraneTriangle mt=new MembraneTriangle(7);
        MembraneCircle mc=new MembraneCircle(1);
        MembraneRectangle mr=new MembraneRectangle(4,2);
        PaperCircle pbc=new PaperCircle(2);

        pt.setColor(Color.BLACK);
        pc.setColor(Color.BLUE);
        pr.setColor(Color.YELLOW);
        pbc.setColor(Color.BLACK);
        listForadd.add(pt);
        listForadd.add(pc);
        listForadd.add(pr);
        listForadd.add(mt);
        listForadd.add(mc);
        listForadd.add(mr);
        listForadd.add(pbc);
        box.addListFigure(listForadd);


        //Выведем содежимое коробки
        System.out.println(box.toString());


        //получим все круги


        System.out.println("Все круги из коробки:\n"+box.getAllCircle().toString());

        //получим все тругольники


        System.out.println("Все тругольники из коробки:\n"+box.getAlltriangle().toString());

        //получим все прямоугольники


        System.out.println("Все прямоугольники из коробки:\n"+box.getAllRectangle().toString());

        //получим все пленочные фигуры


        System.out.println("Все пленочные фигуры из коробки:\n"+box.deleteMembran().toString());

        //получим все красные круги


        System.out.println("Все красные круги из коробки:\n"+box.getAllRedCircle().toString());

        //Удалим список фигур
        List<Figure> listForDelete =new ArrayList<>();
        listForDelete.add(pt);
        listForDelete.add(pc);
        listForDelete.add(pr);
        listForDelete.add(mt);
        box.deletedListFigure(listForDelete);


        //Выведем содежимое коробки
        System.out.println(box.toString());
    }
}
