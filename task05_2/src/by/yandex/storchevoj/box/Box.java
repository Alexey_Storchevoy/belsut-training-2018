package by.yandex.storchevoj.box;

import by.yandex.storchevoj.figure.*;
import by.yandex.storchevoj.figure.specificfigure.PaperCircle;
import by.yandex.storchevoj.material.Membrane;
import by.yandex.storchevoj.material.Paper;

import java.util.*;

/** Класс коробка
 */
public class Box {
    private final ArrayList<Figure> BoxforFigure =new ArrayList<>();
    private final ArrayList<Circle>BoxforCircle=new ArrayList<>();
    private final ArrayList<Membrane>BoxforMembraneFigure=new ArrayList<>();

       public Box(){}
    /** Добавляет фигуру в коробку
     @param figure фигуры для добавления
     @return boolean
     */
    public boolean addBox(Figure figure) {
            if (BoxforFigure.contains(figure)==true || figure==null){
                return false;
            }
            else{
                this.BoxforFigure.add(figure);
                return true;}
    }
    /** Посмотреть фигуру по номеру
     @param number номер фигуры
     @return figure
     */
    public Figure getByNumber(int number){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            return this.BoxforFigure.get(number);
        } else{
            return null;}
    }

    /** Удалить фигуру по номеру
     @param number номер фигуры
     @return boolean
     */
    public boolean deleteforNumber(int number){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            this.BoxforFigure.remove(number);
            return true;
        } else{
            return false;}
    }
    /** Заменить фигуру по номеру
     @param number номер фигуры
     @param figure фигура, на которую заменяем
     @return boolean
     */
    public boolean rewriting(int number,Figure figure){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            this.BoxforFigure.set(number,figure);
            return true;
        } else
            return false;
    }
    /** Количество фигур в коробке
      @return int-количество фигур
     */
    public int numbersofFigure(){
        return this.BoxforFigure.size();
    }
    /** Общая площать фиггур в коробке
      @return double-Общая площать
     */
    public double sumArea(){
        double sum=0;
        for (Figure figure: BoxforFigure) {
            sum+=figure.getArea();
        }
        return sum;
    }
    /** Общий периметр фигур в коробке
     @return double-Общий периметр
     */
    public double sumPerimeter(){
        double sum=0;
        for(Figure figure: BoxforFigure)
        {
            sum+=figure.getPerimeter();
        }
        return sum;
    }
    /** Достать все пленочные фигуры из коробки
     */
    public List<Membrane> deleteMembran(){
        for (Figure figure: BoxforFigure) {
            if (figure instanceof Membrane) {
                this.BoxforMembraneFigure.add((Membrane) figure);}
        }
        return BoxforMembraneFigure;
    }
    /** Достать все круги из коробки
     */
    public void deleteCircle(){
        for (Figure figure: BoxforFigure) {
            if (figure instanceof Circle)  {
                this.BoxforCircle.add((Circle)figure);}
        }
    }
    /** Найти эквивалентную фигуру
     @param figure фигура, которую ищем
     @return figure
     */
    public Figure findInBox(Figure figure){
        for(Figure boxfigure: BoxforFigure)
            if (boxfigure.equals(figure)) {
                return boxfigure;
            }
        return null;
    }
    /** Получить все красные круги

     @return redCircleList
     */
    public List<PaperCircle> getAllRedCircle() {
        List<PaperCircle> redCircleList = new ArrayList();
        for (Figure myFigure : BoxforFigure) {
            if (myFigure instanceof PaperCircle) {
                if (((PaperCircle) myFigure).getColor()== Color.RED) {
                    redCircleList.add((PaperCircle) myFigure);
                }
            }
        }
        return redCircleList;
    }

    /** Переопределенный toString. Выводит все элементы в коробке
     */
    @Override
    public String toString() {
        return  "box=" + BoxforFigure ;
    }
    //получить все круги
    public List<Circle> getAllCircle() {
        List<Circle> сircleList = new ArrayList();
        for (Figure myFigure : BoxforFigure) {
            if (myFigure instanceof Circle) {
                сircleList.add((Circle) myFigure);
            }
        }
        return сircleList;
    }

    //получить все прямоугольники
    public List<Rectangle> getAllRectangle() {
        List<Rectangle> rectangleList = new ArrayList();
        for (Figure myFigure : BoxforFigure) {
            if (myFigure instanceof Rectangle) {
                rectangleList.add((Rectangle) myFigure);
            }
        }
        return rectangleList;
    }

    //получить все тругольники
    public List<Triangle> getAlltriangle() {
        List<Triangle> triangleList = new ArrayList();
        for (Figure myFigure : BoxforFigure) {
            if (myFigure instanceof Triangle) {
                triangleList.add((Triangle) myFigure);
            }
        }
        return triangleList;
    }
    //добавить список фигур
    public boolean addListFigure(List<Figure> listFigure) {
                for (Figure myFigure : listFigure) {
                    if (BoxforFigure.contains(myFigure)) {
                        continue;
                    }
                }
        BoxforFigure.addAll(listFigure);
         return true;
    }

    //достать список фигур
    public boolean deletedListFigure(List<Figure> listFigure) {

            for (Figure myFigure : listFigure) {
                if (!BoxforFigure.contains(myFigure)) {
                    System.out.println("Невозможно достать группу фигур, если хотя бы одной из них нет в коробке");
                return false;
                }
            }
        BoxforFigure.removeAll(listFigure);
         return true;
    }

    public boolean sortByArea(List <Figure> list){
        Collections.sort(list, new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                if(Math.round(o1.getArea()*1000)>Math.round(o2.getArea()*1000))
                    return 1;
                else if(Math.round(o1.getArea()*1000) < Math.round(o2.getArea()*1000))
                    return -1;
                else
                    return 0;
            }
        });
        return true;
    }
    public boolean sortByFigure(List <Figure> list){
        Collections.sort(list, new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                return (o1.getFigureName().compareTo(o2.getFigureName()));
            }
        });
        return true;
    }
    public boolean sortByColor(List <Figure> list){
        Collections.sort(list, new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                Color color1=Color.NOCOLOR;
                Color color2=Color.NOCOLOR;
                if(o1 instanceof Paper ){
                    color1=((Paper) o1).getColor();
                }
                if(o2 instanceof Paper){
                    color2=((Paper) o2).getColor();
                }
                return (color1.toString().compareTo(color2.toString()));
            }
        });
        return true;
    }
    public List<Figure> getBox(){
        return BoxforFigure;
    }


}
