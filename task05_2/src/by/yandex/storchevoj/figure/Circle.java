package by.yandex.storchevoj.figure;


import java.util.Objects;
/** Абстрактный класс круг
  */

public abstract class Circle extends Figure {
    /** Свойство - радиус */
    private double radius;
    /** Создает новый круг
     @param radius радиус
     */
    public Circle(double radius){
        this.radius=radius;
    }
    /** Получает значение свойства radius
     @return Значение свойства radius
     */
    public double getRadius(){
        return radius;
    }
    /** Задает значение свойства radius
     @param radius Новое значение свойства radius
     */
    public void setRadius(double radius){
        this.radius=radius;
    }
    /** Создает новый Круг из другой фигуры
     @param figure фигура
     */
    public Circle(Figure figure){
        this.radius = figure.getMinSize();
    }
    /** Переопределенный getArea.
     * @return площадь
     */
    @Override
    public double getArea(){
        return Math.pow(this.radius,2)*Math.PI;
    }
    /** Переопределенный getPerimeter.
     * @return периметр
     */
    @Override
    public double getPerimeter(){
        return Math.PI*this.radius*2;
    }
    /** Переопределенный toString.
     * @return имя класса+радиус
     */
    @Override
    public String toString(){
        return super.toString()+":radius="+this.radius+",area="+this.getArea();
    }
    /** Переопределенный equals.
     */
    @Override
    public boolean equals(Object o){
        if(!super.equals(o))return false;
        if (this == o) return true;
        Circle circle=(Circle)o;
        return Double.compare(circle.getRadius(),radius) == 0;
    }
    /** Переопределенный hashCode.
     */
    @Override
    public int hashCode(){
    return Objects.hash(getRadius());
    }
    @Override
    public double getMinSize() {
        return radius / 2;
    }
    public String getFigureName(){
        return getClass().getSimpleName();
    }
}
