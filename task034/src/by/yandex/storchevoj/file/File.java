package by.yandex.storchevoj.file;

import by.yandex.storchevoj.box.Box;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class File {
    public static final String fileName = "box.txt";
    public void output(Box box){
        try{
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(box);
            objectOutputStream.close();
        }
        catch (Exception e){
            System.out.println("Problem with output:"+e.getMessage());
        }
    }
    public Box input(){
        Box boxInPut = new Box();
        try{
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            boxInPut = (Box)objectInputStream.readObject();
            objectInputStream.close();
        }
        catch (Exception e){
            System.out.println("Problem with input:"+e.getMessage());
        }
        return boxInPut;
    }
}
