package by.yandex.storchevoj;

import by.yandex.storchevoj.box.Box;
import by.yandex.storchevoj.exeptions.IndexOfofBoundsExeption;
import by.yandex.storchevoj.exeptions.NullFigureExeption;
import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.specificfigure.*;
import by.yandex.storchevoj.file.File;

public class Main {
    public static void main(String[] args) {

        Box box=new Box();

        //Создаем фигуры
        PaperTriangle paperTriangle = new PaperTriangle(4);
        PaperCircle paperCircle=new PaperCircle(5);
        PaperRectangle paperRectangle=new PaperRectangle(4.2,3);
        MembraneTriangle membraneTriangle=new MembraneTriangle(5);
        MembraneCircle membraneCircle=new MembraneCircle(3);
        MembraneRectangle membraneRectangle=new MembraneRectangle(2.5,3.8);
        //Выведем их
        System.out.println("Creatid figuries");

        System.out.println(paperTriangle.toString());
        System.out.println(paperCircle.toString());
        System.out.println(paperRectangle.toString());
        System.out.println(membraneTriangle.toString());
        System.out.println(membraneCircle.toString());
        System.out.println(membraneRectangle.toString());
        //Закрасим бумажные фигуры
        paperTriangle.setColor(Color.BLACK);
        paperCircle.setColor(Color.BLUE);
        paperRectangle.setColor(Color.YELLOW);
        //Выведем их
        System.out.println("Figures after painting");
        System.out.println(paperTriangle.toString());
        System.out.println(paperCircle.toString());
        System.out.println(paperRectangle.toString());

        //Вырезаем фигуры из фигур
        PaperCircle paperCircleFromTriangle=new PaperCircle(paperTriangle);
        PaperTriangle paperTriangleFromRectangle=new PaperTriangle(paperRectangle);
        //Выведем их
        System.out.println("New figures from old");
        System.out.println(paperCircleFromTriangle.toString());
        System.out.println(paperTriangleFromRectangle.toString());
        //Заполняем коробку
        try{
            box.addBox(paperCircle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        //Попробуем добавить финуру второй раз
        try{
            box.addBox(paperCircle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }

        try{
            box.addBox(paperTriangleFromRectangle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }

        try{
            box.addBox(paperCircleFromTriangle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        try{
            box.addBox(membraneCircle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        try{
            box.addBox(membraneRectangle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        try{
            box.addBox(membraneTriangle);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        //Пробуем загрузить null
        System.out.println("Try to add null figure");
        PaperTriangle ptnull=null;
        try{
            box.addBox(ptnull);
        }catch (NullFigureExeption ex){
            System.out.println(ex.getMessage());
        }
        catch (IndexOfofBoundsExeption ex){
            System.out.println(ex.getMessage());
        }
        //Выведем содержимое коробки
        System.out.println("Figures in box");
        System.out.println(box.toString());

        //Сериализация
        File file = new File();
        file.output(box);
        System.out.println("After deserialization");
        //Десериализация
        Box box1;
        box1=file.input();
        System.out.println(box1.toString());
    }
}
