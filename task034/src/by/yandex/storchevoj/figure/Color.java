package by.yandex.storchevoj.figure;

/** Перечисление цветов
 */
    public enum Color{
        WHITE,
        BLACK,
        YELLOW,
        RED,
        GREEN,
        BLUE,
        NOCOLOR;

    }

