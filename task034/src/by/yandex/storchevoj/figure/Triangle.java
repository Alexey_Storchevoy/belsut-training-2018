package by.yandex.storchevoj.figure;

import java.util.Objects;
/** Абстрактный класс треугольник
  */
public abstract class Triangle extends Figure {
    /** Свойство - сторона */
    private double side;
    /** Создает новый треугольник
     @param side сторона
     */
    public Triangle(double side){
        this.side=side;}
    /** Получает значение свойства side
     @return side значение стороны
     */
    public double getSide() {
        return this.side;
    }
    /** Задает значение свойства side
     @param side Новое значение свойства side
     */
    public void setSide(double side){
        this.side=side;}

    /** Создает новый Треугольник из другой фигуры
     @param figure фигура
     */
    public Triangle(Figure figure) {
        this.side = figure.getMinSize();
    }
    /** Переопределенный getPerimeter.
     * @return периметр
     */
    @Override
    public double getPerimeter(){
        return this.side+this.side+this.side;}
    /** Переопределенный getArea.
     * @return площадь
     */
    @Override
    public double getArea() {
        return this.side * this.side * Math.sqrt(3.0) / 4.0;
    }
    /** Переопределенный toString.
     * @return имя класса+сторона
     */
    @Override
    public String toString() {
        return super.toString() + ":side=" + this.side;
    }
    /** Переопределенный equals.
     */
    @Override
    public boolean equals(Object o){
        if(!super.equals(o))return false;
        if (this == o) return true;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.getSide(),side) == 0;
    }
    /** Переопределенный hashCode.
     */
    @Override
    public int hashCode(){
        return Objects.hash(getSide());
    }
    @Override
    public double getMinSize() {
        return side/3;
    }
}
