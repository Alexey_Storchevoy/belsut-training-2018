package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.Triangle;
import by.yandex.storchevoj.material.Membrane;
/** класс пленочный треугольник, реальзует интерфейс Film
 */
public class MembraneTriangle extends Triangle implements Membrane {
    private static final long serialVersionUID = 1L;
    public transient String thatTransientMT="thatTransientMT";
    public static String thatStaticMT="thatStaticMT";
    /** Создает новый пленочный треугольник
     @param side сторона
     */
    public MembraneTriangle(double side) {
        super(side);
    }
    /** Создает новый пленочный треугольник из другой фигуры
     @param membraneFigure пленочная фигура
     */
    public MembraneTriangle(Membrane membraneFigure) {
        super((Figure) membraneFigure);
    }
    public static String getThatStaticMT(){
        return thatStaticMT;
    }

}
