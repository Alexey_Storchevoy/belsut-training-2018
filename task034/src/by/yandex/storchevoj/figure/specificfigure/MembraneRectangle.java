package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.Rectangle;
import by.yandex.storchevoj.material.Membrane;

/** класс пленочный прямоугольник, реальзует интерфейс Film
 */
public class MembraneRectangle extends Rectangle implements Membrane {
    private static final long serialVersionUID = 1L;
    public transient String thatTransientMR="thatTransientMR";
    public static String thatStaticMR="thatStaticMR";
    public static String getThatStaticMR(){
        return thatStaticMR;
    }
    /** Создает новый  пленочный прямоугольник
     @param width сторона
     @param height сторона
     */
    public MembraneRectangle(double width, double height) {
        super(width, height);
    }
    /** Создает новый прямоугольник из другой фигуры
     @param membraneFigure пленочная фигура
     */
    public MembraneRectangle(Membrane membraneFigure) {
        super((Figure) membraneFigure);
    }


}
