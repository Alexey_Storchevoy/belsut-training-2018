package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Circle;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.material.Membrane;
/** Класс пленочный круг, реальзует интерфейс Memgrane
  */
public class MembraneCircle extends Circle implements Membrane {
    private static final long serialVersionUID = 1L;
    public transient String thatTransientMC="thatTransientMC";
    public static String thatStaticMC="thatStaticMC";
    public static String getThatStaticMC(){
        return thatStaticMC;
    }
    /** Создает новый пленочный круг
     @param radius радиус
     */
    public MembraneCircle(double radius) {
        super(radius);
    }
    /** Создает новый пленочный круг из другой фигуры
     @param membraneFigure пленочная фигурв
     */
    public MembraneCircle(Membrane membraneFigure) {
        super((Figure) membraneFigure);
    }
}
