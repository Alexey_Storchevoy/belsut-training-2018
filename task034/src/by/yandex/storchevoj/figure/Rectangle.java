package by.yandex.storchevoj.figure;

import java.util.Objects;
/** Абстрактный класс прямоугольник
 */
public abstract class Rectangle extends Figure {
    /** Свойство - ширина */
    private double width;
    /** Свойство - высота */
    private double height;
    /** Создает новый прямоугольник
     @param width сторона
     @param height сторона
     */
    public Rectangle(double width,double height){
        this.height=height;
        this.width=width;
    }
    /** Создает новый Прямоугольник из другой фигуры
     @param figure фигура
     */
    public Rectangle(Figure figure) {
        this.width = figure.getMinSize();
        this.height = figure.getMinSize()/2;
    }
    /** Получает значение свойства width
     @return Значение свойства width
     */
    public double getWidth(){
        return this.width;}
    /** Получает значение свойства height
     @return Значение свойства height
     */
    public double getHeight(){
        return this.height;}
    /** Задает значение свойства width
     @param width Новое значение свойства width
     */
    public void  setWidth(double width){
        this.width=width;}
    /** Задает значение свойства height
     @param height Новое значение свойства Height
     */
    public void setHeight(double height){
        this.height=height;}
    /** Переопределенный getArea.
     * @return площадь
     */
    @Override
    public double getArea(){
        return this.height*this.width;}
    /** Переопределенный getPerimeter.
     * @return периметр
     */
    @Override
    public double getPerimeter(){
        return this.width*2+this.height*2;}
    /** Переопределенный toString.
     * @return имя класса+ширина+высота
     */
    @Override
    public String toString() {
        return super.toString() + ":width =" + this.width + ",height=" + this.height;
    }
    /** Переопределенный equals.
     */
    @Override
    public boolean equals(Object o){
        if(!super.equals(o))return false;
        if (this == o) return true;
        Rectangle rectangle = (Rectangle) o;
        return ((Double.compare(rectangle.getHeight(),height))+(Double.compare(rectangle.getWidth(),width)))==0;
    }
    /** Переопределенный hashCode.
     */
    @Override
    public int hashCode(){
        return Objects.hash(getHeight(),height);
    }
    @Override
    public double getMinSize() {
        return Math.min(width,height)/2;
    }
}
