package by.yandex.storchevoj;

import by.yandex.storchevoj.box.Box;
import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.specificfigure.*;

public class Main {
    public static void main(String[] args) {

        Box box=new Box();

        //Создаем фигуры
        PaperTriangle paperTriangle = new PaperTriangle(4);
        PaperCircle paperCircle=new PaperCircle(5);
        PaperRectangle paperRectangle=new PaperRectangle(4.2,3);
        MembraneTriangle membraneTriangle=new MembraneTriangle(5);
        MembraneCircle membraneCircle=new MembraneCircle(3);
        MembraneRectangle membraneRectangle=new MembraneRectangle(2.5,3.8);
        //Выведем их
        System.out.println("Creatid figuries");

        System.out.println(paperTriangle.toString());
        System.out.println(paperCircle.toString());
        System.out.println(paperRectangle.toString());
        System.out.println(membraneTriangle.toString());
        System.out.println(membraneCircle.toString());
        System.out.println(membraneRectangle.toString());
        //Закрасим бумажные фигуры
        paperTriangle.setColor(Color.BLACK);
        paperCircle.setColor(Color.BLUE);
        paperRectangle.setColor(Color.YELLOW);
        //Выведем их
        System.out.println("Figures after painting");
        System.out.println(paperTriangle.toString());
        System.out.println(paperCircle.toString());
        System.out.println(paperRectangle.toString());

        //Вырезаем фигуры из фигур
        PaperCircle paperCircleFromTriangle=new PaperCircle(paperTriangle);
        PaperTriangle paperTriangleFromRectangle=new PaperTriangle(paperRectangle);
        //Выведем их
        System.out.println("New figures from old");
        System.out.println(paperCircleFromTriangle.toString());
        System.out.println(paperTriangleFromRectangle.toString());
        //Заполняем коробку
        box.addBox(paperCircle);
        //Попробуем добавить финуру второй раз
        box.addBox(paperCircle);
        box.addBox(paperTriangleFromRectangle);
        box.addBox(paperCircleFromTriangle);
        box.addBox(membraneCircle);
        box.addBox(membraneRectangle);
        box.addBox(membraneTriangle);
        //Выведем содержимое коробки
        System.out.println("Figures in box");
        System.out.println(box.toString());
        //Найдем фигуру по номеру
        System.out.println("figure by number:"+box.getByNumber(1));;
        //Удалим фигуру по номеру
        box.deleteforNumber(1);
        //Выведем содежимое коробки
        System.out.println(box.toString());
        //Создадим фигуру которую перезапишем на место ранее записаной
        MembraneTriangle treangleForRewriting=new MembraneTriangle(10);
        System.out.println("figure for rewriting"+treangleForRewriting.toString());
        //Перезапишем
        box.rewriting(1,treangleForRewriting);
        //Выведем содержимое коробки
        System.out.println(box.toString());
        //Создадим фигуру с параметроми одной из фигур коробки
        System.out.println("Creating new figure with the parameters one of the figures ");
        MembraneTriangle likeOneOffigure= new MembraneTriangle(10);
        System.out.println(likeOneOffigure.toString());
        //Попробуем найти фигуру с одинаковыми параметрами
        System.out.println("Equivalent:"+box.findInBox(likeOneOffigure));;
        //Выведем количество фигур
        System.out.println("Numbers of figures in box: "+box.numbersofFigure());
        //Найдем суммарную площадь всех фигур в коробки
        System.out.println("Sum area:"+box.sumArea());
        //Найдем суммарный периметр всех фигур в коробки
        System.out.println("Sum perimetre:"+box.sumPerimeter());
        //Достаним все круги
        box.deleteCircle();
        //Достаним все пленочные фигуры
        box.deleteMembran();

    }
}
