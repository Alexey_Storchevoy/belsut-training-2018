package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.Rectangle;
import by.yandex.storchevoj.material.Paper;

import java.util.Objects;
/** Класс бумажный прямоугольник, реальзует интерфейс Paper
 */
public class PaperRectangle extends Rectangle implements Paper {
    /** Цвет фигуры
     */
    private Color color=Color.NOCOLOR;
    /** Создает новый бумажный прямоугольник
     @param width ширина
     @param height высота
     */
    public PaperRectangle(double width, double height) {
        super(width, height);
    }
    /** Создает новый бумажный прямоугольник
     @param paperFigure бумажная фигура
     */
    public PaperRectangle(Paper paperFigure){
        super((Figure)paperFigure);
        this.setColor(paperFigure.getColor());
    }
    /** Задает цвет фигуры
     * @param  color цвет
     */
    @Override
    public void setColor(Color color) {
        if(this.color==Color.NOCOLOR)this.color=color;
    }
    /** Получает цвет фигуры
     * @return Color
     */
    @Override
    public Color getColor(){
        return this.color;
    }
    /** Переопределенный toString.
     * @return имя класса+ширина+высота+материал+цвет
     */
    @Override
    public String toString(){
        return super.toString()+":color="+this.color;
    }
    /** Переопределенный equals
     */
    @Override
    public boolean equals(Object o){
        if (!super.equals(o))return false;
        if(this==o)return true;
        PaperRectangle that=(PaperRectangle) o;
        return Objects.equals(color,that.color);
    }
    /** Переопределенный hashCode
     */
    @Override
    public int hashCode(){
        return Objects.hash(super.hashCode(),getColor());
    }

}
