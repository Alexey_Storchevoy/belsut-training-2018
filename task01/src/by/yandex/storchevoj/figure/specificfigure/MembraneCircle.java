package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Circle;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.material.Membrane;
/** Класс пленочный круг, реальзует интерфейс Memgrane
  */
public class MembraneCircle extends Circle implements Membrane {
    /** Создает новый пленочный круг
     @param radius радиус
     */
    public MembraneCircle(double radius) {
        super(radius);
    }
    /** Создает новый пленочный круг из другой фигуры
     @param membraneFigure пленочная фигурв
     */
    public MembraneCircle(Membrane membraneFigure) {
        super((Figure) membraneFigure);
    }
}
