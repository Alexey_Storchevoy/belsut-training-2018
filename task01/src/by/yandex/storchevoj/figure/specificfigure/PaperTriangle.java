package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.Triangle;
import by.yandex.storchevoj.material.Paper;

import java.util.Objects;
/** Класс бумажный треугольник, реальзует интерфейс Paper
 */
public class PaperTriangle extends Triangle implements Paper {
    /** Цвет фигуры
     */
    private Color color=Color.NOCOLOR;
    /** Создает новый  бумажный треугольник
     @param side  сторона
     */
    public PaperTriangle(double side) {
        super(side);
    }
    /** Создает новый  бумажный треугольник
     @param paperFigure бумажная фигура
     */
    public PaperTriangle(Paper paperFigure){
        super((Figure)paperFigure);
        this.setColor(paperFigure.getColor());
    }
    /** Задает цвет фигуры
     * @param  color цвет
     */
    @Override
    public void setColor(Color color) {
        if(this.color==Color.NOCOLOR)this.color=color;
    }
    /** Получает цвет фигуры
     * @return Color
     */
    @Override
    public Color getColor(){
        return this.color;
    }


    /** Переопределенный toString.
     * @return имя класса+сторона+материал+цвет
     */
    @Override
    public String toString(){
        return super.toString()+":color="+this.color;
    }
    /** Переопределенный equals
     */
    @Override
    public boolean equals(Object o){
        if (!super.equals(o))return false;
        if(this==o)return true;
        PaperTriangle that=(PaperTriangle) o;
        return Objects.equals(color,that.color);
    }
    /** Переопределенный hashCode
     */
    @Override
    public int hashCode(){
        return Objects.hash(super.hashCode(),getColor());
    }
}
