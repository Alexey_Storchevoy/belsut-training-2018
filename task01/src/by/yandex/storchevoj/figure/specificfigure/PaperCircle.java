package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Circle;
import by.yandex.storchevoj.figure.Color;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.material.Paper;

import java.util.Objects;
/** Класс бумажный круг, реальзует интерфейс Paper
 */
public class PaperCircle extends Circle implements Paper {
    /** Цвет фигуры
     */
    private Color color=Color.NOCOLOR;
    /** Создает новый бумажный круг
     @param radius радиус
     */
    public PaperCircle(double radius) {
        super(radius);
    }
    /** Создает новый бумажный круг
     @param paperFigure бумажная фигура
     */
    public PaperCircle(Paper paperFigure){
        super((Figure)paperFigure);
        this.setColor(paperFigure.getColor());
    }

    /** Задает цвет фигуры
     * @param  color цвет
     */
    @Override
    public void setColor(Color color) {
        if(this.color==Color.NOCOLOR){
            this.color=color;}
    }
    /** Получает цвет фигуры
     * @return color
     */
    @Override
    public Color getColor(){
        return this.color;
    }
    /** Переопределенный toString.
     /
     * @return имя класса+радиус+материал+цвет
     */
    @Override
    public String toString(){
        return super.toString()+":color="+this.color;
    }
    /** Переопределенный equals
     */
    @Override
    public boolean equals(Object o){
        if (!super.equals(o))return false;
        if(this==o)return true;
        PaperCircle that=(PaperCircle) o;
        return Objects.equals(color,that.color);
    }
    /** Переопределенный hashCode
     */
    @Override
    public int hashCode(){
        return Objects.hash(super.hashCode(),color);
    }
}
