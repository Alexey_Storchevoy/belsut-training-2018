package by.yandex.storchevoj.figure.specificfigure;

import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.figure.Triangle;
import by.yandex.storchevoj.material.Membrane;
/** класс пленочный треугольник, реальзует интерфейс Film
 */
public class MembraneTriangle extends Triangle implements Membrane {
    /** Создает новый пленочный треугольник
     @param side сторона
     */
    public MembraneTriangle(double side) {
        super(side);
    }
    /** Создает новый пленочный треугольник из другой фигуры
     @param membraneFigure пленочная фигура
     */
    public MembraneTriangle(Membrane membraneFigure) {
        super((Figure) membraneFigure);
    }


}
