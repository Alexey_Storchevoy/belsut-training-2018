package by.yandex.storchevoj.figure;
/** Абстрактный класс фигур
  */
public abstract class Figure {
    public Figure(){}
    /** Метод возвращает площадь фигуры, без реализации
     */
    public abstract double getArea();
    /** Метод возвращает периметр фигуры, без реализации
     */
    public abstract double getPerimeter();

    /** Переопределенный toString.
     * @return имя класса
     */
    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
    @Override
    public boolean equals(Object o){
        if(o==null){
            return false;
        }
        return getClass()==o.getClass();
    }
    public abstract double getMinSize();
   }

