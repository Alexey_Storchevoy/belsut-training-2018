package by.yandex.storchevoj.box;

import by.yandex.storchevoj.figure.Circle;
import by.yandex.storchevoj.figure.Figure;
import by.yandex.storchevoj.material.Membrane;

import java.util.ArrayList;
/** Класс коробка
 */
public class Box {
    private final ArrayList<Figure> BoxforFigure =new ArrayList<>();
    private final ArrayList<Circle>BoxforCircle=new ArrayList<>();
    private final ArrayList<Membrane>BoxforMembraneFigure=new ArrayList<>();

       public Box(){}
    /** Добавляет фигуру в коробку
     @param figure фигуры для добавления
     @return boolean
     */
    public boolean addBox(Figure figure) {
            if (BoxforFigure.contains(figure)==true || figure==null){
                return false;
            }
            else{
                this.BoxforFigure.add(figure);
                return true;}
    }
    /** Посмотреть фигуру по номеру
     @param number номер фигуры
     @return figure
     */
    public Figure getByNumber(int number){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            return this.BoxforFigure.get(number);
        } else{
            return null;}
    }

    /** Удалить фигуру по номеру
     @param number номер фигуры
     @return boolean
     */
    public boolean deleteforNumber(int number){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            this.BoxforFigure.remove(number);
            return true;
        } else{
            return false;}
    }
    /** Заменить фигуру по номеру
     @param number номер фигуры
     @param figure фигура, на которую заменяем
     @return boolean
     */
    public boolean rewriting(int number,Figure figure){
        if(number<=this.BoxforFigure.size()-1 && number>=0) {
            this.BoxforFigure.set(number,figure);
            return true;
        } else
            return false;
    }
    /** Количество фигур в коробке
      @return int-количество фигур
     */
    public int numbersofFigure(){
        return this.BoxforFigure.size();
    }
    /** Общая площать фиггур в коробке
      @return double-Общая площать
     */
    public double sumArea(){
        double sum=0;
        for (Figure figure: BoxforFigure) {
            sum+=figure.getArea();
        }
        return sum;
    }
    /** Общий периметр фигур в коробке
     @return double-Общий периметр
     */
    public double sumPerimeter(){
        double sum=0;
        for(Figure figure: BoxforFigure)
        {
            sum+=figure.getPerimeter();
        }
        return sum;
    }
    /** Достать все пленочные фигуры из коробки
     */
    public void deleteMembran(){
        for (Figure figure: BoxforFigure) {
            if (figure instanceof Membrane) {
                this.BoxforMembraneFigure.add((Membrane) figure);}
        }
    }
    /** Достать все круги из коробки
     */
    public void deleteCircle(){
        for (Figure figure: BoxforFigure) {
            if (figure instanceof Circle)  {
                this.BoxforCircle.add((Circle)figure);}
        }
    }
    /** Найти эквивалентную фигуру
     @param figure фигура, которую ищем
     @return figure
     */
    public Figure findInBox(Figure figure){
        for(Figure boxfigure: BoxforFigure)
            if (boxfigure.equals(figure)) {
                return boxfigure;
            }
        return null;
    }

    /** Переопределенный toString. Выводит все элементы в коробке
     */
    @Override
    public String toString() {
        return  "box=" + BoxforFigure ;
    }
}
