package by.yandex.storchevoj.material;

import by.yandex.storchevoj.figure.Color;
/** Интерфейс бумага
 */
public interface Paper {
    /** Задает значения свойства color,без реализации
     */
void setColor(Color color);
    /** Получает значение свойства color,без реализации
     */
Color getColor();
}